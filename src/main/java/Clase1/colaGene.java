package Clase1;
public class colaGene <T>{
    private Nodo<T> primero;
    private Nodo<T> ultimo;

    public Cola(){
    primero=null;
    ultimo=null;}
    
    public boolean empty(){
        if(primero ==null) {return true;}
        else {return false;}}

    public void push(T x){
    Nodo<T> nuevo = new Nodo(x,null);
    if(primero==null) {primero=nuevo;}
    else{ultimo.setSiguiente(nuevo);}
    ultimo=nuevo;}

    public T dele(){
    T x;
    Nodo<T> temp;
    x=primero.getInfo();
    temp=primero;
    primero=primero.getSiguiente();
    temp=null;
    if (primero==null) {ultimo=null;}
    return x;}

    public void showAll(){
    Nodo<T> p=primero;
    while(p!=null){
            System.out.println(p.getInfo());
             p=p.getSiguiente();}}
}
